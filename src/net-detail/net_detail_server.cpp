#include "daqi/net-detail/net_detail_server.hpp"

namespace da4qi4
{
namespace net_detail
{

SocketInterface::~SocketInterface() {}
Socket::~Socket() {}
SocketWithSSL::~SocketWithSSL() {}

template <typename T>
IOC_EXECUTOR get_ioc_executor_from(T& socket_obj)
{
#ifdef HAS_IO_CONTEXT
    return socket_obj.get_executor();
#else
    return socket_obj.get_io_service();
#endif
}

IOC_EXECUTOR Socket::get_ioce()
{
    return get_ioc_executor_from(_socket);
}

IOC_EXECUTOR SocketWithSSL::get_ioce()
{
    return get_ioc_executor_from(_stream);
}

} //namespace net_detail
} // namespace da4qi4
